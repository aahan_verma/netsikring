package com.netsikring.di.component

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.netsikring.application.MainApplication
import com.netsikring.di.module.ApplicationModule
import com.netsikring.dns.DNSService
import com.netsikring.utils.RxBus
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {
    fun dnsChangerApp(): MainApplication
    fun rxBus(): RxBus
    fun appContext(): Context
    fun pref(): SharedPreferences
    fun gson(): Gson
    fun inject(service: DNSService)
}