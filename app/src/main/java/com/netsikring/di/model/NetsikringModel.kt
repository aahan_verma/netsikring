package com.netsikring.di.model

data class ToolBarConfiguration(val resMenu:Int?, val heading:String?)