package com.netsikring.di.prefs

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.netsikring.model.ConfigurationsData
import com.netsikring.model.UserInformation

class PrefUtils {

    private fun getSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences("Netsikring_Prefs", Context.MODE_PRIVATE)
    }

    fun storeAuthKey(context: Context, apiKey: String) {
        val editor = getSharedPreferences(context).edit()
        editor.putString("AUTH_KEY", apiKey)
        editor.apply()
    }

    fun getAuthKey(context: Context): String? {
        return getSharedPreferences(context).getString("AUTH_KEY", null)
    }

    fun isUserLoggedIn(context: Context, isLoggedIn: Boolean) {
        val editor = getSharedPreferences(context).edit()
        editor.putBoolean("isUserLoggedIn", isLoggedIn)
        editor.apply()
    }

    fun getIsUserLoggedIn(context: Context): Boolean? {
        return getSharedPreferences(context).getBoolean("isUserLoggedIn", false)
    }

    fun isReceiverConnected(context: Context, receiverstatus: Boolean) {
        val editor = getSharedPreferences(context).edit()
        editor.putBoolean("isReceiverConnected", receiverstatus)
        editor.apply()
    }

    fun getReceiverConnected(context: Context): Boolean {
        return getSharedPreferences(context).getBoolean("isReceiverConnected", false)
    }

    fun clearPrefs(context: Context) {
        val sharedPrefs = context.getSharedPreferences("Netsikring_Prefs", Context.MODE_PRIVATE)
        val editor = sharedPrefs.edit()
        editor.clear()
        editor.apply()
    }

    fun setIsPaymentDone(context: Context, receiverstatus: Boolean) {
        val editor = getSharedPreferences(context).edit()
        editor.putBoolean("isPaymentDone", receiverstatus)
        editor.apply()
    }

    fun getIsPaymentDone(context: Context): Boolean {
        return getSharedPreferences(context).getBoolean("isPaymentDone", false)
    }


    fun storeFirebaseToken(context: Context, userId: String) {
        val editor = getSharedPreferences(context).edit()
        editor.putString("FCM_TOK", userId)
        editor.apply()
    }

    fun getFirebaseToken(context: Context): String? {
        return getSharedPreferences(context).getString("FCM_TOK", null)
    }

    fun setUserPin(context: Context, userPin: String) {
        val editor = getSharedPreferences(context).edit()
        editor.putString("userPin", userPin)
        editor.apply()
    }

    fun getUserPin(context: Context): String? {
        return getSharedPreferences(context).getString("userPin", "")
    }

    fun setEmail(context: Context, userPin: String) {
        val editor = getSharedPreferences(context).edit()
        editor.putString("email", userPin)
        editor.apply()
    }

    fun getEmail(context: Context): String? {
        return getSharedPreferences(context).getString("email", "")
    }

    fun setUserCreateAt(context: Context, userPin: String) {
        val editor = getSharedPreferences(context).edit()
        editor.putString("user_created_at", userPin)
        editor.apply()
    }

    fun getUserCreateAt(context: Context): String? {
        return getSharedPreferences(context).getString("user_created_at", "")
    }

    fun setConfigurations(context: Context, userData: ConfigurationsData) {
        val gson = Gson()
        val userDataString = gson.toJson(userData)
        val editor = getSharedPreferences(context).edit()
        editor.putString("ConfigurationsData", userDataString)
        editor.apply()
    }

    fun getConfigurations(context: Context): ConfigurationsData? {
        val gson = Gson()
        return gson.fromJson(
            getSharedPreferences(context)
                .getString("ConfigurationsData", null), ConfigurationsData::class.java
        )

    }

    fun setUserInformation(context: Context, userData: UserInformation) {
        val gson = Gson()
        val userDataString = gson.toJson(userData)
        val editor = getSharedPreferences(context).edit()
        editor.putString("UserInformation", userDataString)
        editor.apply()
    }

    fun getUserInformation(context: Context): UserInformation? {
        val gson = Gson()
        return gson.fromJson(
                getSharedPreferences(context)
                        .getString("UserInformation", null), UserInformation::class.java
        )

    }

}