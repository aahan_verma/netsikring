package com.netsikring.di.module

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.netsikring.application.MainApplication
import com.netsikring.utils.RxBus
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
@Singleton
class ApplicationModule(val application: MainApplication) {

    @Provides
    @Singleton
    fun provideApplication(): MainApplication {
        return application
    }

    @Provides
    @Singleton
    fun rxBus(): RxBus {
        return RxBus()
    }

    @Provides
    @Singleton
    fun providesSharedPreferences(application: MainApplication?): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(application)
    }

    @Provides
    @Singleton
    fun gson(): Gson {
        return GsonBuilder().create()
    }

    @Provides
    @Singleton
    fun provideApplicationContext(): Context {
        return application.applicationContext
    }

}