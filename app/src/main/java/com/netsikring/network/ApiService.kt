package com.netsikring.network

import com.google.gson.JsonObject
import com.netsikring.model.GetTokenResponse
import io.reactivex.Single
import retrofit2.http.*

interface ApiService {

    @GET("get-token")
    fun getToken(): Single<JsonObject>

    @GET("get-configurations")
    fun getConfiguration(): Single<JsonObject>

    @FormUrlEncoded
    @POST("login")
    fun doLogin(@FieldMap map :HashMap<String,Any>): Single<JsonObject>

    @FormUrlEncoded
    @POST("update-password")
    fun changePassword(@FieldMap map :HashMap<String,Any>): Single<JsonObject>

    @FormUrlEncoded
    @POST("register")
    fun register(@FieldMap map :HashMap<String,Any>): Single<JsonObject>

    @FormUrlEncoded
    @POST("update-vpn-status")
    fun updateVpnStatus(@FieldMap map :HashMap<String,Any>): Single<JsonObject>
}