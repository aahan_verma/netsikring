package com.netsikring.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import com.netsikring.R
import com.netsikring.base.BaseActivity
import com.netsikring.model.GetTokenResponse
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject

class ApiConnector<T>(
    private val mContext: BaseActivity<*>,
    private val service: Single<JsonObject>,
    private val pojoClassType: Class<T>) {

    internal fun initApi(mCallBack: onApiCallBack<T>) {
        if (isOnline(mContext)) {
            mContext.getDisposable()!!.add(
                service.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : DisposableSingleObserver<JsonObject>() {
                        override fun onSuccess(t: JsonObject) {
                            if (t.size() != 0) {
                                mCallBack.onSuccess(Gson().fromJson(t, pojoClassType))
                            } else {
                                mCallBack.onFailure(mContext.getString(R.string.internet_server_error_message))
                            }
                        }

                        override fun onError(e: Throwable) {
                            errorHandling(e)
                        }
                    })
            )
        } else {
            mCallBack.onFailure("Please check your internet connectivity")
        }
    }


    interface onApiCallBack<T> {
        fun onSuccess(response: T)
        fun onFailure(message: String)
    }

    fun errorHandling(e: Throwable) {
        try {
            val obj = JSONObject((e as HttpException).response().errorBody()!!.string())
            val errorMessage = obj.optJSONObject("error").optString("message")
            val errorCode = e.code()
            Toast.makeText(mContext, "Something Went Wrong", Toast.LENGTH_LONG).show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    val UN_AUTHORIZED = 401
    val INTERNAL_SERVER_ERROR = 500
    val BAD_REQUEST = 400
    val SUSSPENDED = 406


    private fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val nw = connectivityManager.activeNetwork ?: return false
            val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return false
            return when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            val nwInfo = connectivityManager.activeNetworkInfo ?: return false
            return nwInfo.isConnected
        }
    }
}