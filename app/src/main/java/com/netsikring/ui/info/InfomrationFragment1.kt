package com.netsikring.ui.info

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.View
import androidx.fragment.app.Fragment
import com.netsikring.R
import com.netsikring.databinding.FragmentInformation1Binding
import com.netsikring.databinding.FragmentWelcomeBinding
import com.netsikring.di.model.ToolBarConfiguration
import com.netsikring.model.ConfigurationsData
import com.netsikring.ui.login.LoginFragment
import com.netsikring.ui.netsikring.NetsikringFragment

class InfomrationFragment1 : NetsikringFragment<FragmentInformation1Binding>() {
    override fun getLayoutId(): Int {
        return R.layout.fragment_information_1
    }

    override fun getCurrentFragment(): Fragment? {
        return this@InfomrationFragment1
    }

    override fun showToolbar(): Boolean? {
        return false
    }

    override fun configureToolbar(): ToolBarConfiguration? {
        return null
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getViewDataBinding().btnNext.setOnClickListener {
            replaceFragment(LoginFragment(), true, true)
        }
        setData(getNetsikringPrefs()!!.getConfigurations(getContainerActivity()) )
    }


    private fun setData(configurations: ConfigurationsData?) {
        var message= ""
        configurations!!.list!!.withIndex().map {
            if (it.value.keyName!!.equals("info_page",true)){
                message = it.value!!.value!!
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                    getViewDataBinding()!!.tvMessage.text = Html.fromHtml(message, Html.FROM_HTML_MODE_LEGACY)
                }else{
                    getViewDataBinding()!!.tvMessage.text = Html.fromHtml(message)
                }

            }
        }

        getViewDataBinding().rlMain.animate().alpha(1f)
    }
}