package com.netsikring.ui.welcome

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.View
import androidx.fragment.app.Fragment
import com.netsikring.R
import com.netsikring.databinding.FragmentWelcomeBinding
import com.netsikring.di.model.ToolBarConfiguration
import com.netsikring.model.ConfigurationsData
import com.netsikring.model.GetConfigurationResponse
import com.netsikring.network.ApiConnector
import com.netsikring.ui.info.InfomrationFragment1
import com.netsikring.ui.netsikring.NetsikringFragment

class FragmentWelcome : NetsikringFragment<FragmentWelcomeBinding>() {
    override fun getLayoutId(): Int {
        return R.layout.fragment_welcome
    }

    override fun getCurrentFragment(): Fragment? {
        return this@FragmentWelcome
    }

    override fun showToolbar(): Boolean? {
        return false
    }

    override fun configureToolbar(): ToolBarConfiguration? {
        return null
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getConfigurationApi()
      /*  if (getNetsikringPrefs()!!.getConfigurations(getContainerActivity()) == null) {

        } else {
            setData(getNetsikringPrefs()!!.getConfigurations(getContainerActivity()) )
        }*/
        getViewDataBinding().btnNext.setOnClickListener {
            replaceFragment(InfomrationFragment1(), true, true)
        }
    }

    private fun setData(configurations: ConfigurationsData?) {
        var message= ""
        configurations!!.list!!.withIndex().map {
            if (it.value.keyName!!.equals("welcome_page",true)){
                message = it.value!!.value!!
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                    getViewDataBinding()!!.tvMessage.text = Html.fromHtml(message,Html.FROM_HTML_MODE_LEGACY)
                }else{
                    getViewDataBinding()!!.tvMessage.text = Html.fromHtml(message)
                }

            }
        }

        getViewDataBinding().rlPrent.animate().alpha(1f)
    }

    private fun getConfigurationApi() {
        val api = getApiService()!!.getConfiguration()
        val connector = ApiConnector<GetConfigurationResponse>(
            getContainerActivity(),
            api,
            GetConfigurationResponse::class.java
        )
        showLoader()
        connector.initApi(object : ApiConnector.onApiCallBack<GetConfigurationResponse> {
            override fun onSuccess(response: GetConfigurationResponse) {
                hideLoading()
                getNetsikringPrefs()!!.setConfigurations(
                    getContainerActivity(),
                    response.configurations!!
                )
                setData(response.configurations!!)
            }

            override fun onFailure(message: String) {
                hideLoading()
                showError(message)
            }
        })
    }
}