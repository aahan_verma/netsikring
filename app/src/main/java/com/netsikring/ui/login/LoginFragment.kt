package com.netsikring.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.netsikring.R
import com.netsikring.databinding.FragmentLoginBinding
import com.netsikring.di.model.ToolBarConfiguration
import com.netsikring.model.LoginResponse
import com.netsikring.network.ApiConnector
import com.netsikring.ui.home.HomeActivity
import com.netsikring.ui.netsikring.NetsikringFragment
import com.netsikring.ui.profile.CreateProfileFragment

class LoginFragment : NetsikringFragment<FragmentLoginBinding>() {
    override fun getLayoutId(): Int {
        return R.layout.fragment_login
    }

    override fun getCurrentFragment(): Fragment? {
        return this@LoginFragment
    }

    override fun showToolbar(): Boolean? {
        return false
    }

    override fun configureToolbar(): ToolBarConfiguration? {
        return null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getViewDataBinding().btnNext.setOnClickListener {
            if (isEmailValid(
                    getViewDataBinding().etEmail,
                    "Indtast gyldig email adresse"
                ) &&
                validateEditText(getViewDataBinding().etPassword, "PIN-kode")
            ) {
                // Replace Fragment
                login()

            }
        }

        getViewDataBinding().tvSignUp.setOnClickListener {
            replaceFragment(CreateProfileFragment(), true, true)
        }
    }

    private fun login() {
        generateToken(object : NetsikringFragment.onTokenGenrateCallBack {
            override fun getToken(token: String) {
                val map = HashMap<String, Any>()
                map["_token"] = token
                map["email"] = getViewDataBinding().etEmail.text.toString().trim()
                map["password"] = getViewDataBinding().etPassword.text.toString().trim()

                val api = getApiService()!!.doLogin(map)
                val connector = ApiConnector<LoginResponse>(
                    getContainerActivity(),
                    api,
                    LoginResponse::class.java
                )
                connector.initApi(object : ApiConnector.onApiCallBack<LoginResponse> {
                    override fun onSuccess(response: LoginResponse) {
                        hideLoading()

                        if (response.success!!) {
                            showToast("Logget ind")
                            val date = dateFormatConverter(response!!.user!!.created_at!!)
                            getNetsikringPrefs()!!.setUserCreateAt(getContainerActivity()!!, date!!)
                            getNetsikringPrefs()!!.isUserLoggedIn(getContainerActivity(), true)
                            getNetsikringPrefs()!!.setUserInformation(
                                getContainerActivity(),
                                response!!.user!!
                            )
                            getNetsikringPrefs()!!.setEmail(
                                getContainerActivity(),
                                response!!.user!!.email!!
                            )
                            getNetsikringPrefs()!!.setUserPin(
                                getContainerActivity(),
                                getViewDataBinding().etPassword.text.toString().trim()
                            )
                            val intent = Intent(getContainerActivity(), HomeActivity::class.java)
                            getContainerActivity().startActivity(intent)
                            getContainerActivity().overridePendingTransition(
                                R.anim.fade_in,
                                R.anim.fade_out
                            )
                            getContainerActivity().finishAffinity()
                        } else {
                            showError("Ugyldig email eller PIN-kode")
                        }
                    }

                    override fun onFailure(message: String) {
                        showError(message)
                        hideLoading()
                    }
                })
            }
        })
    }
}