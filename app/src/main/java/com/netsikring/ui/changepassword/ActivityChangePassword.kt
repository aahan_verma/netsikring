package com.netsikring.ui.changepassword

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import com.netsikring.R
import com.netsikring.base.BaseActivity
import com.netsikring.databinding.ActivityChangePasswordBinding
import com.netsikring.databinding.DialogErrorBinding
import com.netsikring.model.GetTokenResponse
import com.netsikring.network.ApiConnector
import com.netsikring.ui.netsikring.NetsikringDialog
import com.netsikring.ui.netsikring.NetsikringFragment
import com.ntpc.app.base.DialogBinding

class ActivityChangePassword : BaseActivity<ActivityChangePasswordBinding>() {
    override fun getLayoutId(): Int {
        return R.layout.activity_change_password
    }

    override fun getCurrentActivity(): Activity? {
        return this@ActivityChangePassword
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getViewDataBinding().ivNav.setOnClickListener {
            onBackPressed()
        }

        getViewDataBinding().btnNext.setOnClickListener {
            if (validateEditText(
                    getViewDataBinding().etPassword,
                    "PIN-kode"
                ) && validateEditText(
                    getViewDataBinding().etCPassword,
                    "Indtast bekræftelsesnål",
                )
                && comparePassword(
                    getViewDataBinding().etPassword,
                    getViewDataBinding().etCPassword
                )
            ) {
                changePassword()
            }
        }
    }

    private fun changePassword() {
        generateToken(object : NetsikringFragment.onTokenGenrateCallBack {
            override fun getToken(token: String) {
                val map = HashMap<String, Any>()
                map["_token"] = token
                map["email"] = getNetsikringPrefs()!!.getEmail(this@ActivityChangePassword)!!
                map["old_password"] =
                    getNetsikringPrefs()!!.getUserPin(this@ActivityChangePassword)!!
                map["new_password"] = getViewDataBinding().etPassword.text.toString()

                val api = getApiService()!!.changePassword(map)
                val connector = ApiConnector<GetTokenResponse>(
                    this@ActivityChangePassword,
                    api,
                    GetTokenResponse::class.java
                )
                connector.initApi(object : ApiConnector.onApiCallBack<GetTokenResponse> {
                    override fun onSuccess(response: GetTokenResponse) {
                        runOnUiThread {
                            Toast.makeText(this@ActivityChangePassword,"PIN-koden er opdateret",Toast.LENGTH_LONG).show()
                            getNetsikringPrefs()!!.setUserPin(
                                    this@ActivityChangePassword,
                                    getViewDataBinding().etPassword.text.toString()
                            )!!
                            hideLoader()
                            onBackPressed()
                        }

                    }

                    override fun onFailure(message: String) {
                        hideLoader()
                    }
                })
            }
        })
    }

    fun validateEditText(et: EditText, msg: String): Boolean {
        return if (et.text.toString().trim() == "") {
            showErrorDialog(msg)
            false
        } else
            true
    }

    fun comparePassword(etOne: EditText, etTwo: EditText): Boolean {

        if (etOne.text.toString() == etTwo.text.toString()) {
            return true
        } else {
//            etTwo.requestFocus()
//            etTwo.error = "Password and Confirm password doesn't match"
            showErrorDialog("Indtast ens PIN-koder")
            return false
        }
    }

    fun showErrorDialog(message: String) {
        NetsikringDialog<DialogErrorBinding>(
            this@ActivityChangePassword,
            R.layout.dialog_error,
            object : DialogBinding<DialogErrorBinding> {
                override fun onBind(binder: DialogErrorBinding, dialog: Dialog) {
                    binder.tvMessage.text = message
                    binder.btnClick.text = "OK"
                    binder.btnClick.setOnClickListener {
                        dialog.dismiss()
                    }
                }
            }).show(supportFragmentManager, NetsikringDialog::class.java.simpleName)
    }

    fun generateToken(mCallback: NetsikringFragment.onTokenGenrateCallBack) {
        showLoader()
        val api = getApiService()!!.getToken()
        val connector = ApiConnector<GetTokenResponse>(
            this@ActivityChangePassword,
            api,
            GetTokenResponse::class.java
        )
        connector.initApi(object : ApiConnector.onApiCallBack<GetTokenResponse> {
            override fun onSuccess(response: GetTokenResponse) {
                mCallback.getToken(response.token!!)
            }

            override fun onFailure(message: String) {
                hideLoader()
            }
        })
    }
}