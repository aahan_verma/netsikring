package com.netsikring.ui.netsikring

import android.app.Activity
import android.app.Dialog
import android.app.NotificationManager
import android.content.Intent
import android.net.VpnService
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.google.gson.Gson
import com.netsikring.R
import com.netsikring.application.MainApplication
import com.netsikring.base.BaseActivity
import com.netsikring.databinding.ActivityNetsikringBinding
import com.netsikring.databinding.DialogErrorBinding
import com.netsikring.databinding.DialogLogoutBinding
import com.netsikring.di.model.ToolBarConfiguration
import com.netsikring.dns.DNSModule
import com.netsikring.dns.DNSPresenter
import com.netsikring.dns.DNSPresenter.Companion.SERVICE_OPEN
import com.netsikring.dns.DaggerDNSComponent
import com.netsikring.dns.IDNSView
import com.netsikring.model.DNSModel
import com.netsikring.ui.home.HomeFragment
import com.netsikring.ui.login.LoginFragment
import com.netsikring.ui.welcome.FragmentWelcome
import com.netsikring.utils.AppCommon
import com.netsikring.utils.appinterface.onMessageButtonClickDialog
import com.netsikring.utils.hide
import com.netsikring.utils.loadGlide
import com.netsikring.utils.picker.ImagePickerActivity
import com.netsikring.utils.show
import com.ntpc.app.base.DialogBinding
import javax.inject.Inject

open class NetsikringActivity : BaseActivity<ActivityNetsikringBinding>(), AppNavigationInterface{
    override fun getLayoutId(): Int {
        return R.layout.activity_netsikring
    }

    var fragmentTransaction: FragmentTransaction? = null


    override fun getCurrentActivity(): Activity? {
        return this@NetsikringActivity
    }

    fun disableAutoFill() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                window.decorView.importantForAutofill =
                    View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        disableAutoFill()
        disableAutoFill()

        ImagePickerActivity.clearCache(this@NetsikringActivity)

        replaceFragment(FragmentWelcome(), true, true)



    }




    override fun replaceFragment(fragment: Fragment, addToStack: Boolean, allowAnim: Boolean) {
        mHandler!!.post {
            currentFragment = fragment
            fragmentTransaction = supportFragmentManager.beginTransaction()
            if (addToStack) {
                fragmentTransaction!!.addToBackStack(fragment::class.java.simpleName)
            }
            if (allowAnim) {
                fragmentTransaction!!.setCustomAnimations(
                    R.anim.enter,
                    R.anim.exit,
                    R.anim.enter_from_left,
                    R.anim.exit_to_right
                )
            }
            fragmentTransaction!!.replace(
                R.id.flContent,
                currentFragment!!,
                fragment::class.java.simpleName
            ).commitAllowingStateLoss()
        }
    }


    override fun setArguments(mFragment: Fragment, mBundle: Bundle): Fragment {
        mFragment.arguments = mBundle
        return mFragment
    }

    override fun showErrorDialog(message: String, listener: onMessageButtonClickDialog) {
        NetsikringDialog<DialogErrorBinding>(
            this@NetsikringActivity,
            R.layout.dialog_error,
            object : DialogBinding<DialogErrorBinding> {
                override fun onBind(binder: DialogErrorBinding, dialog: Dialog) {
                    dialog.setCanceledOnTouchOutside(false)
                    dialog.setCancelable(false)
                    binder.tvMessage.text = message
                    binder.btnClick.text = "Okay"
                    binder.btnClick.setOnClickListener {
                        listener.onDialogClick()
                        dialog.dismiss()
                    }
                }
            }).show(supportFragmentManager, NetsikringDialog::class.java.simpleName)
    }

    override fun showErrorDialog(message: String) {
        NetsikringDialog<DialogErrorBinding>(
            this@NetsikringActivity,
            R.layout.dialog_error,
            object : DialogBinding<DialogErrorBinding> {
                override fun onBind(binder: DialogErrorBinding, dialog: Dialog) {
                    binder.tvMessage.text = message
                    binder.btnClick.text = "OK"
                    binder.btnClick.setOnClickListener {
                        dialog.dismiss()
                    }
                }
            }).show(supportFragmentManager, NetsikringDialog::class.java.simpleName)
    }

    override fun showToast(message: String) {
        Toast.makeText(this@NetsikringActivity, message, Toast.LENGTH_SHORT).show()
    }

    fun setToolbarVisible(showToolbar: Boolean?) {
        if (showToolbar!!) {
            getViewDataBinding().rlToolbar.show()
        } else {
            getViewDataBinding().rlToolbar.hide()
        }
    }


    fun configurationToolbar(configureToolbar: ToolBarConfiguration?) {
        if (configureToolbar != null) {
            getViewDataBinding().tvHeading.text = configureToolbar.heading
            if (configureToolbar.resMenu != -1) {
                getViewDataBinding().ivNav.show()
                loadGlide(
                    configureToolbar.resMenu,
                    getViewDataBinding().ivNav,
                    this@NetsikringActivity
                )
            } else {
                getViewDataBinding().ivNav.hide()
            }
        }
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (currentFragment is HomeFragment) {

        }
        super.onActivityResult(requestCode, resultCode, data)
        //currentFragment?.onActivityResult(requestCode, resultCode, data)
    }



}