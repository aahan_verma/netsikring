package com.netsikring.ui.netsikring

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import com.ntpc.app.base.DialogBinding

class NetsikringDialog<T : ViewDataBinding>(val activity: AppCompatActivity, val resId: Int, val binder : DialogBinding<T>) :DialogFragment() {

    private var mViewDataBinding: T? = null
    var mView: View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (mViewDataBinding ==null) {
            mViewDataBinding = DataBindingUtil.inflate<T>(LayoutInflater.from(activity), resId, null, false)
        }
        mView = mViewDataBinding?.root
        return mView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binder.onBind(mViewDataBinding!!,dialog!!)


    }

//    override fun getTheme(): Int {
//        if (resId==R.layout.inner_adapter_product_image) {
//            return R.style.DialogTheme
//        }
//        else {
//            return 0
//        }
//    }

    override fun onStart() {
        super.onStart()
      /*  dialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )*/
        dialog?.window?.setBackgroundDrawable(
            ColorDrawable(
                Color.TRANSPARENT
            )
        );
    }
}