package com.netsikring.ui.netsikring

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.format.DateUtils
import android.view.WindowManager
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.netsikring.base.BaseFragment
import com.netsikring.model.GetTokenResponse
import com.netsikring.network.ApiConnector
import com.netsikring.utils.appinterface.onMessageButtonClickDialog
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

abstract class NetsikringFragment<T : ViewDataBinding?> : BaseFragment<T>(), FragmentView {
    private var mContainerActivity: NetsikringActivity? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContainerActivity = context as NetsikringActivity
    }

    fun getContainerActivity(): NetsikringActivity {
        return mContainerActivity!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getContainerActivity().window.setSoftInputMode(
            WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN
        )
    }

    override fun onStart() {
        super.onStart()
        mContainerActivity?.setToolbarVisible(showToolbar())
        mContainerActivity?.setRunningFragment(getCurrentFragment())
        mContainerActivity?.configurationToolbar(configureToolbar())
    }

    fun showError(message: String, listener: onMessageButtonClickDialog) {
        this.mContainerActivity?.showErrorDialog(message, listener)
    }

    fun showError(message: String) {
        this.mContainerActivity?.showErrorDialog(message)
    }


    /**
     * Disable input of zero at start of edit Text
     * */
    fun disableZeroInput(editText: EditText) {
        editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length == 1 && (s.toString().startsWith("0")
                            || s.toString().startsWith("."))
                ) {
                    s!!.clear()
                }
            }
        })
    }

    fun disableSpaceInput(editText: EditText) {
        editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {
                val textEntered = editText.text.toString()

                if (textEntered.isNotEmpty() && textEntered.contains(" ")) {
                    editText.setText(editText.text.toString().replace(" ", ""))
                    editText.setSelection(editText.text!!.length);
                }
            }
        })

    }

    fun setCharLimit(editText: EditText, tvCount: TextView) {
        editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                val length = s.toString().length
                if (length != 0) {
                    val remainingChars = 300 - length
                    tvCount.text = "($remainingChars/300 characters)"
                } else {
                    tvCount.text = "(300 characters)"
                }
            }
        })
    }


    fun validateString(et: String, msg: String): Boolean {
        return if (et.trim() == "") {
            showError(msg)
            false
        } else
            true
    }

    fun validateSpinner(et: Spinner, msg: String): Boolean {
        return if (et.selectedItemPosition == 0) {
            showError(msg)
            false
        } else
            true
    }

    fun validateEditText(et: EditText, msg: String): Boolean {
        return if (et.text.toString().trim() == "") {
            showError(msg)
            false
        } else
            true
    }

    fun comparePassword(etOne: EditText, etTwo: EditText): Boolean {

        if (etOne.text.toString() == etTwo.text.toString()) {
            return true
        } else {
            showError("Indtast ens PIN-koder")
            return false
        }
    }

    fun isEmailValid(et: EditText, msg: String): Boolean {
        val isValid =
            android.util.Patterns.EMAIL_ADDRESS.matcher(et.text.toString().trim()).matches()

        return if (isValid) {
            true
        } else {
            showError(msg)
            false
        }
    }

    fun validatePhoneNumber(et: EditText): Boolean {

        return if (et.text.toString().length < 8) {
            et.requestFocus()
            showError("Please enter valid phone number")
            false
        } else
            true
    }


    fun replaceFragment(frg0: Fragment, isBack: Boolean, showAnim: Boolean) {
        mContainerActivity!!.replaceFragment(frg0, isBack, showAnim)
    }

    fun setArguments(mFragment: Fragment, mBundle: Bundle): Fragment {
        return mContainerActivity?.setArguments(mFragment, mBundle)!!
    }


    fun showToast(message: String) {
        getContainerActivity().showToast(message)
    }

    @SuppressLint("NewApi")
    fun dateFormatConverter(actualDate: String?): String? {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        inputFormat.timeZone = TimeZone.getTimeZone("UTC");
        val outputFormat = SimpleDateFormat("yyyy-MM-dd")
        val date: Date = inputFormat.parse(actualDate)
        return outputFormat.format(date)
    }

    fun setTimeDayAgo(actualDate: String): String? {
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"))
        try {
            val time: Long = sdf.parse(actualDate).getTime()
            val now = System.currentTimeMillis()
            var ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS)
            if (ago.toString().equals("0 minutes ago")) {
                return "just now"
            } else {
                if (ago.toString().contains("minutes", true)) {
                    return ago.toString().replace("minutes", "min")
                }
                return ago.toString()
            }

        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return ""
    }

    interface onTokenGenrateCallBack {
        fun getToken(token: String)
    }


    fun generateToken(mCallback: onTokenGenrateCallBack) {
        showLoader()
        val api = getApiService()!!.getToken()
        val connector = ApiConnector<GetTokenResponse>(
            getContainerActivity(),
            api,
            GetTokenResponse::class.java
        )
        connector.initApi(object : ApiConnector.onApiCallBack<GetTokenResponse> {
            override fun onSuccess(response: GetTokenResponse) {
                mCallback.getToken(response.token!!)
            }

            override fun onFailure(message: String) {
                showError(message)
                hideLoading()
            }
        })
    }

    fun getCurrentUTCDate(): String? {
        val format = "yyyy-MM-dd kk:mm:ss"
        val sdf = SimpleDateFormat(format)
        sdf.timeZone = TimeZone.getTimeZone("UTC");
        val dateInMillis = System.currentTimeMillis()
        val date = sdf.format(Date(dateInMillis))
        return date
    }
}