package com.netsikring.ui.netsikring

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.netsikring.utils.appinterface.onMessageButtonClickDialog

interface AppNavigationInterface {

    fun replaceFragment(fragment: Fragment, addToStack: Boolean, showAnimation: Boolean)

    fun setArguments(fragment: Fragment, bundle: Bundle):Fragment

    /**
     * With Call back
     * */
    fun showErrorDialog(message: String, listener: onMessageButtonClickDialog)

    /**
     * With out call back
     * */
    fun showErrorDialog(message: String)

    fun showToast(message: String)
}