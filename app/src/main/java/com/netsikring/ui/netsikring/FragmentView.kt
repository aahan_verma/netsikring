package com.netsikring.ui.netsikring

import androidx.fragment.app.Fragment
import com.netsikring.di.model.ToolBarConfiguration

interface FragmentView {

    fun getCurrentFragment():Fragment?

    fun showToolbar():Boolean?

    fun configureToolbar(): ToolBarConfiguration?
}