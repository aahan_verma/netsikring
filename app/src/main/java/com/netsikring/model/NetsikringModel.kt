package com.netsikring.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GetTokenResponse {
    val token: String? = null
    val success: Boolean? = null
    val message: String? = null
}

class LoginResponse {
    val success: Boolean? = null
    val user: UserInformation? = null
    val message: String? = null
    val error: String? = null
}

class UserInformation {
    val id: Int? = null
    val name: String? = null
    val email: String? = null
    val created_at: String? = null
}

/***
 * Configuration list
 * */


class ConfigurationList {
    @SerializedName("group_name")
    @Expose
    var groupName: String? = null

    @SerializedName("key_name")
    @Expose
    var keyName: String? = null

    @SerializedName("is_image")
    @Expose
    var isImage: Int? = null

    @SerializedName("value")
    @Expose
    var value: String? = null
}

class ConfigurationsData {
    @SerializedName("list")
    @Expose
    var list: List<ConfigurationList>? = null
}

class GetConfigurationResponse {
    @SerializedName("configurations")
    @Expose
    var configurations: ConfigurationsData? = null

    @SerializedName("baseURL")
    @Expose
    var baseURL: String? = null
}