package com.netsikring.utils.event

import com.netsikring.model.DNSModel


class ServiceInfo(model: DNSModel) {
    private var model: DNSModel
    fun getModel(): DNSModel {
        return model
    }

    fun setModel(model: DNSModel) {
        this.model = model
    }

    init {
        this.model = model
    }
}