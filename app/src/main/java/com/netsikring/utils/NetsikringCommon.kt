package com.netsikring.utils

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import com.netsikring.R

class NetsikringCommon {
    companion object{
        fun setLoadingDialog(activity: AppCompatActivity): Dialog {

            val dialog = Dialog(activity)
            dialog.show()
            if (dialog.window != null) {
                dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            }
            dialog.setContentView(R.layout.loader)
            dialog.setCancelable(false)
            dialog.setCanceledOnTouchOutside(false)
            return dialog
        }
    }

}