package com.netsikring.utils

import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.netsikring.R

object AppCommon {
    fun showExitWarning(layout: ViewGroup, activity: AppCompatActivity) {
        val snack = Snackbar.make(
            layout,
            "To exit, Tap back button again.",
            Snackbar.LENGTH_SHORT
        ).setBackgroundTint(ContextCompat.getColor(activity, R.color.purple_700))
        snack.setActionTextColor(ContextCompat.getColor(activity, R.color.white))
        snack.setAction("EXIT NOW") {
            activity.finish()
        }
        val snackBarView = snack.view
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity, R.color.purple_700))
        snack.show()
    }
}