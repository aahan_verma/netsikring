package com.netsikring.utils

import android.content.Context
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import java.io.File

fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide() {
    visibility = View.GONE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun loadGlide(resId: Int?, img: ImageView, context: Context) {
    Glide.with(context).load(resId).into(img)
}
fun loadGlide(resId: Int?, img: ImageView, context: Context, placeholder: Int) {
    Glide.with(context).load(resId).placeholder(placeholder).into(img)
}

fun loadGlide(resId: String?, img: ImageView, context: Context) {
    Glide.with(context).load(resId).into(img)
}

fun loadGlide(resId: String?, img: ImageView, context: Context, placeholder: Int) {
    Glide.with(context).load(resId).placeholder(placeholder).into(img)
}

fun loadGlide(resId: File?, img: ImageView, context: Context) {
    Glide.with(context).load(resId).into(img)
}


