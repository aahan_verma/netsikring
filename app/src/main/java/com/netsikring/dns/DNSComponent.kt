package com.netsikring.dns

import com.netsikring.MainActivity
import com.netsikring.base.BaseActivity
import com.netsikring.di.component.ApplicationComponent
import com.netsikring.di.scope.ActivityScope
import com.netsikring.ui.home.HomeActivity
import com.netsikring.ui.netsikring.NetsikringActivity
import dagger.Component

@Component(modules = [DNSModule::class], dependencies = [ApplicationComponent::class])
@ActivityScope
interface DNSComponent {
    fun view(): IDNSView
    fun inject(activity: HomeActivity)
}