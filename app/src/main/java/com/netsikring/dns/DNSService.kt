package com.netsikring.dns

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.VpnService
import android.os.ParcelFileDescriptor
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import com.netsikring.R
import com.netsikring.application.MainApplication
import com.netsikring.model.DNSModel
import com.netsikring.utils.RxBus
import com.netsikring.utils.event.GetServiceInfo
import com.netsikring.utils.event.ServiceInfo
import com.netsikring.utils.event.StartEvent
import com.netsikring.utils.event.StopEvent
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import java.io.IOException
import java.net.InetSocketAddress
import java.nio.channels.DatagramChannel
import javax.inject.Inject

class DNSService : VpnService() {
    @Inject
    lateinit var rxBus: RxBus

    @Inject
    lateinit var context: Context

    @Inject
    lateinit var gson: Gson
    private val builder: Builder = Builder()
    private var fileDescriptor: ParcelFileDescriptor? = null
    private var mThread: Thread? = null
    private var shouldRun = true
    private var tunnel: DatagramChannel? = null
    private var dnsModel: DNSModel? = null
    private var preferences: SharedPreferences? = null
    private var subscriber: Disposable? = null
    private fun stopThisService() {
        shouldRun = false
        stopSelf()
    }

    override fun onDestroy() {
        super.onDestroy()
       /* preferences!!.edit().putBoolean("isStarted", false).apply()
        preferences!!.edit().remove("dnsModel").apply()

        subscriber!!.dispose()*/
    }

    override fun onCreate() {
        super.onCreate()
        preferences = PreferenceManager.getDefaultSharedPreferences(this)
        MainApplication.getApplicationComponent()?.inject(this)
        subscribe()
    }

    private fun subscribe() {
        subscriber = rxBus?.events.subscribe(Consumer<Any?> { o ->
            if (o is StopEvent) {
                stopThisService()
            } else if (o is GetServiceInfo) {
                rxBus?.sendEvent(ServiceInfo(dnsModel!!))
            }
        })
    }

    private fun setTunnel(tunnel: DatagramChannel) {
        this.tunnel = tunnel
    }

    private fun setFileDescriptor(fileDescriptor: ParcelFileDescriptor?) {
        this.fileDescriptor = fileDescriptor
    }

    override fun onStartCommand(paramIntent: Intent, p1: Int, p2: Int): Int {
        mThread = Thread({
            try {
                dnsModel = paramIntent.getParcelableExtra(DNS_MODEL)
                val modelJSON: String = gson!!.toJson(dnsModel)
                preferences!!.edit().putString("dnsModel", modelJSON).apply()
                setFileDescriptor(
                    builder.setSession(
                        this@DNSService.getText(R.string.app_name).toString()
                    ).addAddress("192.168.0.1", 24).addDnsServer(dnsModel?.firstDns!!)
                        .addDnsServer(dnsModel!!.secondDns!!).establish()
                )
                setTunnel(DatagramChannel.open())
                tunnel!!.connect(InetSocketAddress("127.0.0.1", 8087))
                protect(tunnel!!.socket())
                while (shouldRun) Thread.sleep(100L)
            } catch (exception: Exception) {
                exception.printStackTrace()
            } finally {
                if (fileDescriptor != null) {
                    try {
                        fileDescriptor?.close()
                        setFileDescriptor(null)
                    } catch (e: IOException) {

                    }
                }
            }
        }, "DNS Changer")
        mThread!!.start()
        rxBus?.sendEvent(StartEvent())
        preferences!!.edit().putBoolean("isStarted", true).apply()
        return START_STICKY
    }

    companion object {
        const val DNS_MODEL = "DNSModelIntent"
    }
}