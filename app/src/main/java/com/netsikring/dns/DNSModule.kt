package com.netsikring.dns

import com.netsikring.di.scope.ActivityScope
import dagger.Module
import dagger.Provides

@Module
@ActivityScope
class DNSModule(private val idnsView: IDNSView) {
    @Provides
    @ActivityScope
    fun idnsView(): IDNSView {
        return idnsView
    }
}