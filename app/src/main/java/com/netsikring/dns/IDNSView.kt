package com.netsikring.dns

import com.netsikring.model.DNSModel


interface IDNSView {
    fun changeStatus(serviceStatus: Int)
    fun setServiceInfo(model: DNSModel?)
}