package com.netsikring.dns

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.preference.PreferenceManager
import com.netsikring.MainActivity
import com.netsikring.ui.home.HomeActivity

class BootReceiver : BroadcastReceiver() {
    private var notificationManager: NotificationManager? = null
    private var notificationBuilder: NotificationCompat.Builder? = null
    override fun onReceive(context: Context, intent: Intent) {
            val preferences: SharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context)
            val autoStart: Boolean = preferences.getBoolean("startBoot", true)
            val isStarted: Boolean = preferences.getBoolean("isStarted", false)
            val dnsModelJSON: String = preferences.getString("dnsModel", "")!!
          // if (isStarted) {
                notificationManager =
                        context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                sendNotification(context, dnsModelJSON,intent)
            //}

    }

    private fun sendNotification(context: Context, dnsModel: String, intent: Intent) {
      /*  val intentAction = Intent(context, HomeActivity::class.java)
        intentAction.putExtra("dnsModel", dnsModel)
        val pendingIntent: PendingIntent =
            PendingIntent.getActivity(context, 1, intentAction, PendingIntent.FLAG_ONE_SHOT)
        notificationBuilder = NotificationCompat.Builder(context)
            .setSmallIcon(R.drawable.ic_launcher_background)
            .setContentTitle(context.getString(R.string.service_ready))
            .setContentIntent(pendingIntent)
            .addAction(
                R.mipmap.ic_launcher,
                context.getString(R.string.turn_on),
                pendingIntent
            )
            .setAutoCancel(true)
        val notification: Notification = notificationBuilder!!.build()
        notificationManager!!.notify(1903, notification)*/
        if (Intent.ACTION_BOOT_COMPLETED == intent.getAction()) {
            val activityIntent = Intent(context, HomeActivity::class.java)
            activityIntent.putExtra("dnsModel", dnsModel)
            activityIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(activityIntent)
        }
    }
}