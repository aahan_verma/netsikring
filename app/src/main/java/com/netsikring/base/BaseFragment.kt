package com.netsikring.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.netsikring.di.prefs.PrefUtils
import com.netsikring.network.ApiService
import io.reactivex.disposables.CompositeDisposable

abstract class BaseFragment<T : ViewDataBinding?> : Fragment() {

    private var mActivity: BaseActivity<*>? = null
    private var mViewDataBinding: T? = null
    private var mRootView: View? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (mViewDataBinding == null) {
            mViewDataBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
            mRootView = mViewDataBinding!!.root
        }
        return mRootView
    }


    @LayoutRes
    abstract fun getLayoutId(): Int

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity<*>) {
            mActivity = context
        }
    }

    override fun onDetach() {
        mActivity = null
        super.onDetach()
    }

    fun getViewDataBinding(): T {
        return mViewDataBinding!!
    }

    fun getNetsikringPrefs(): PrefUtils? {
        return mActivity?.getNetsikringPrefs()
    }

    open fun getDisposable(): CompositeDisposable? {
        return mActivity!!.getDisposable()
    }

    open fun getApiService(): ApiService? {
        return mActivity!!.getApiService()
    }

    fun showLoader() {
        try {
            mActivity?.showLoader()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun hideLoading() {
        try {
            mActivity?.hideLoader()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}